CFLAGS=-Wall -std=c++11 -Ofast -fgcse-sm -fgcse-las -fipa-pta -ftree-ch -DNDEBUG
SRCS=src/input.cpp src/types.cpp src/strategy.cpp src/solver.cpp src/utils.cpp
OBJS=${SRCS:.cpp=.o}
CC=g++

debug: CFLAGS = -Wall -Werror -std=c++1y -DDEBUG -g
debug: all

all: solver solution

solver: ${OBJS}
	${CC} ${CFLAGS} src/main.cpp ${OBJS} -o solver

solution: ${OBJS}
	${CC} ${CFLAGS} src/solution.cpp ${OBJS} -o solution

.cpp.o:
	${CC} ${CFLAGS} -c $< -o $@

clean:
	rm -fr src/*.o solution solver libSolver.a

.PHONY: all clean solver solution
