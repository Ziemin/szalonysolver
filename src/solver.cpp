#include "solver.hpp"
#include "utils.hpp"
#include "types.hpp"

SolverState initSolverState(CNF cnf) {
  cnf = simplifyCNF(cnf);
  PBindings values(findMax(cnf) + 1, 0);
  int assigned = 0;
  values[0] = 100;
  for (int v = 1; v < (int)values.size(); v++) {
    values[v] = v;
    assigned++;
  }
  // 1 dostaną zmienne nigdzie już nie występujące
  for (const Clause& clause: cnf)
    for (Literal lit: clause) {
      if (values[variable(lit)] != 0) {
        values[variable(lit)] = 0;
        assigned--;
      }
    }

  std::vector<int> levels(values.size(), -1);
  std::vector<ClauseID> reasons(values.size(), NO_ID);

  return SolverState {
    move(cnf),
    CNFLearnt(),
    0,
    0,
    move(values),
    move(levels),
    {},
    move(reasons),
    assigned,
    0,
    0,
    0
  };
}

Watches::Watches(const CNF& cnf, int varCount)
    : std::vector<Watched>(2*(varCount+1))
{
  for (int cind = 0; cind < (int)cnf.size(); cind++) {
    if (cnf[cind].size() >= 2) {
      (*this)[-cnf[cind][0]].push_back({cind, false});
      (*this)[-cnf[cind][1]].push_back({cind, false});
    }
  }
}

Watched& Watches::operator[](Literal lit) {
  return std::vector<Watched>::operator[](size()/2 + lit);
}

const Watched& Watches::operator[](Literal lit) const {
  return std::vector<Watched>::operator[](size()/2 + lit);
}

void Watches::removeClause(const Clause& cl, ClauseID cid) {
  assert(cl.size() >= 2);
  for (int i = 0; i < 2; i++) {
    Watched& w = (*this)[-cl[i]];
    int last = 0;
    for (int j = 0; j < (int)w.size(); j++) {
      if (cid != w[j]) {
        w[last] = w[j];
        last++;
      }
    }
    w.pop_back();
  }
}

void Watches::addClause(const Clause& cl, ClauseID cid) {
  for (int i = 0; i < 2; i++) {
    (*this)[-cl[i]].push_back(cid);
  }
}
