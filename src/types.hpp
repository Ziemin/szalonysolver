#pragma once

#include <vector>
#include <iostream>
#include <cmath>
#include <string>
#include <utility>
#include <cassert>

using Variable = int;
using Literal = int;
inline Variable variable(Literal lit) { return std::abs(lit); }
constexpr inline bool sign(int lit) { return lit >= 0; }

// -- ujemny - false, dodatni - true, 0 - brak wartości
using Bind = int;

// Pełny model
using FBindings = std::vector<bool>;

// Częściowy model
using PBindings = std::vector<Bind>;

inline bool valueFor(Literal lit, bool bind) {
  return sign(lit) == bind;
}

inline bool valueFor(Literal lit, Bind bind) {
  assert(bind != 0);
  return sign(lit) == sign(bind);
}

inline bool valueFor(Literal lit, const FBindings& binds) {
  return valueFor(lit, binds[variable(lit)]);
}

inline bool valueFor(Literal lit, const PBindings& binds) {
  return valueFor(lit, binds[variable(lit)]);
}

constexpr inline Literal assumeVar(Variable var, bool value) {
  return var * (value ? 1 : -1);
}

// Reprezentacja klauzuli
struct Clause : public std::vector<Literal> {

  // zwraca logiczną wartość klauzli dla pewnego wartościowania
  bool operator()(const FBindings& binds) const;

  friend std::ostream& operator<<(std::ostream&, const Clause& clause);
};

struct ClauseID {
  int ind;
  bool learnt;
  bool empty;

  constexpr inline ClauseID(int ind, bool learnt) : ind(ind), learnt(learnt), empty(false) { }
  constexpr inline ClauseID() : ind(0), learnt(false), empty(true) { }

  inline bool isNull() const { return empty; }
  inline bool operator==(const ClauseID& other) const {
    if (empty != other.empty) return false;
    if (empty && other.empty) return true;
    return ind == other.ind && learnt == other.learnt;
  }
  inline bool operator!=(const ClauseID& other) const {
    return !(*this == other);
  }
  inline bool operator<(const ClauseID& other) const {
    if (empty) return true;
    return this->ind < other.ind;
  }
};

// Reprezentacja całego problemu CNF
struct CNF : public std::vector<Clause> {

  // zwraca logiczną wartość CNF dla pewnego wartościowania
  bool operator()(const FBindings& binds) const;

  friend std::ostream& operator<<(std::ostream&, const CNF& clause);
};

inline bool isSatisfied(Literal lit, const FBindings& binds) {
    if (binds[variable(lit)] == 0) return false;
    return valueFor(lit, binds);
}
bool isSatisfied(const Clause& cnf, const FBindings& binds);
bool isSatisfied(const CNF& cnf, const FBindings& binds);
bool isSatisfied(const Clause& cnf, const PBindings& binds);
bool isSatisfied(const CNF& cnf, const PBindings& binds);

constexpr int NO_LEVEL = -1;
constexpr ClauseID NO_ID = ClauseID();

struct CNFLearnt : public CNF {

  // operatory dostawania się do klauzul po ich id
  // iterowanie się normalnie może zwracać klauzule w zupełnie innej kolejności
  Clause& operator[](ClauseID cid);
  const Clause& operator[](ClauseID cid) const;
  using CNF::operator[]; // bez tego ukrylibyśmy normalny operator po intach

  // id klauzuli po usunięciu przestaje być poprawne
  // może zostać przejęte później przez nowo dodaną klauzulę
  void removeClauses(std::vector<ClauseID> toRemove);
  ClauseID addClause(Clause clause);

  private:
    std::vector<int> freeInds;
};

struct SolverState {
  CNF formula;                   // oryginalny problem
  CNFLearnt learnt;                    // zbiór nauczonych klauzul
  int decLevel;                  // obecny poziom decyzyjny
  int rootLevel;                 // obecny poziom decyzyjny
  PBindings values;              // obecne wartościowanie
  std::vector<int> levels;       // poziomy przypisania zmiennych
  std::vector<Literal> trail;    // stos przypisań
  std::vector<ClauseID> reasons; // klauzule wymuszające wartości zmiennych
  int assignedCount;             // liczba zawartościowanych zmiennych
  int conflicts;                 // ile wystąpiło konfliktów od ostatniego restartu
  int learntCount;               // Liczba nauczonych klauzul
  int restarts;                  // Liczba wykonanych już restartów

  inline int varCount() const {
    return values.size() - 1;
  }
  inline bool hasValue(Variable var) const {
    return values[var] != 0;
  }
  inline bool isTrue(Literal lit) const {
    return values[variable(lit)] == lit;
  }
  inline bool isFalse(Literal lit) const {
    return values[variable(lit)] == -lit;
  }
  inline bool isLocked(ClauseID cid) const {
    assert(!cid.isNull());
    if (!cid.learnt)
      return true;
    const int var = variable(learnt[cid][0]);
    return reasons[var] == cid;
  }

};

struct SolverError {
  inline SolverError() : lit(0), cid(NO_ID) { }
  inline SolverError(Literal lit, ClauseID cid) : lit(lit), cid(cid) { }

  Literal lit;
  ClauseID cid;
  inline operator bool() const { return lit != 0; }
};
extern SolverError NO_ERROR;

// Reprezentuje kolejną akcję solvera
enum class Action {
  CONTINUE,
  RESTART,
  SAY_NO
};
