#pragma once

#include "strategy.hpp"

// ---  variable decisions  -------------------------------------;

DecisionS::VSIDS::Init vsidsConf = {
  1.11,          // decay
  1000000,      // threshold
  0.000001,     // rescaler
  1.3,           // randPercent
  false,        // useLearned
  true           // initBySize
};

// ---  polarity -------------------------------------;

DecisionS::PhaseSavingPolarity::Init phaseSavingConf = {
  false,         // clear backups
  true,        // clear statistics
  false,          // clear conflicting
  false,         // use random
};

DecisionS::JerWangPolarity::Init jerWangConf = { };

// ---  learning -------------------------------------;

LearnS::FUIP::Init learnConf = { };

// ---  removals -------------------------------------;

RemovalS::Activity::Init activityConf = {
  1.13,          // decay
  10000000,      // threshold
  0.0000001,     // rescaler
  0.55           // removeFraction
};

RemovalS::LBD::Init lbdConf = {
  0.6,            // removeFraction
  false           // updateTouched
};

RemovalS::BigOld::Init bigOldConf = {
  0.65,           // removeFraction
  6               // updateTouched
};
// ---  schedules for removals -------------------------------------;

RemovalS::DynamicScheduling::Init dynamicSchedConf = {
  0.53,          // maxLearnts
  1.1            // incMaxLearnts
};

RemovalS::GeometricScheduling::Init geometricSchedConf = {
  100,           // maxConfl
  1.5            // incMaxLearnts
};

RemovalS::StaticScheduling::Init staticSchedConf = {
  2000,          // b
  500            // a
};

// ---  restarts ----------------------------------------------

ActionS::Geometric::Init geometricConf = {
  150,           // maxConflicts
  1.5,           // confMult
  0              // say no time in seconds
};

ActionS::NestedGeometric::Init nestedConf = {
  100,           // inner limit begin
  250,           // outer limit begin
  1.5,           // conf mult
};

ActionS::Luby::Init lubyConf = {
  100,           // factor
  90
};

// ---  minimiazations ----------------------------------------------

MinimizationS::NoMinimization::Init minConf = { };

// ---  setup ----------------------------------------------

using DecVarType = DecisionS::VSIDS;
using DecPolarityType = DecisionS::PhaseSavingPolarity;
using DecType = DecisionS::Decision<DecVarType, DecPolarityType>;

using ReduceType = RemovalS::Activity;
using SchedType = RemovalS::DynamicScheduling;
using RemovalType = RemovalS::RemoveStrategy<SchedType, ReduceType>;

using LearnType = LearnS::FUIP;

using MinType = MinimizationS::NoMinimization;

using ActType = ActionS::Luby;

using StratType = ComposedStrategy<DecType, LearnType, RemovalType, ActType, MinType>;


DecType::Init decConf = {
  vsidsConf,
  phaseSavingConf
};

RemovalType::Init removalConf = {
  dynamicSchedConf,
  activityConf
};

ActType::Init actConf = lubyConf;

StratType::Init stratConf = {
  decConf, learnConf, removalConf, actConf, minConf
};
