#include <iostream>
#include <cassert>

#include "types.hpp"
#include "input.hpp"
#include "strategy.hpp"
#include "solver.hpp"
#include "config.hpp"

int main() {

  CNF cnf = readProblem(std::cin);
  Solver<StratType> solver(cnf, std::move(stratConf));

  PBindings result = solver.solve();
  if (result.empty()) {
    std::cout << "S UNSAT" << std::endl;
  } else {
    assert(isSatisfied(cnf, result));
    std::cout << "S SATISFIABLE" << std::endl << "v ";
    for (int i = 1; i < (int)result.size(); i++) {
      if (result[i]) std::cout << result[i] << " ";
    }
    std::cout << std::endl;
  }

  return 0;
}
