#pragma once

#include <tuple>
#include <vector>
#include <random>
#include <unordered_set>
#include <chrono>
#include "types.hpp"

/* ------------ Sygnatura strategii -----------------
 *
 *  Strategy::Init - typ który reprezetuje dane potrzebne do inicjalizacji
 *                   np. jakieś parametry konfiguracji
 *
 *  Literal decide() {
 *  std::tuple<Clause, int> analyse(SolverError)
 *  void handleNewClause(ClauseID id)
 *  std::vector<ClauseID> reduceDB()
 *  Action nextAction()
 *  void handleAction(Action action)
 *
 *
 **/

/*
 * Strategia reprezentująca kompozycję osobnych strategii odpowiedzialnych za:
 * - decydowanie o kolejnej wartościowanej zmiennej
 * - analiza konfliktu i zarzadzanie nauczonymi klauzulami
 * - podejmowanie akcji solvera
 */
template <typename DecisionStrat, typename LearnStrat, typename RemovalStrat, typename ActionStrat, typename MinimizerStrat>
struct ComposedStrategy {

  struct Init {
    typename DecisionStrat::Init dinit;
    typename LearnStrat::Init linit;
    typename RemovalStrat::Init rinit;
    typename ActionStrat::Init ainit;
    typename MinimizerStrat::Init minit;
  };

  ComposedStrategy(const SolverState& solverState, ComposedStrategy::Init init)
    : dec(solverState, std::move(init.dinit))
    , learn(solverState, std::move(init.linit))
    , removal(solverState, std::move(init.rinit))
    , act(solverState, std::move(init.ainit))
    , mins(solverState, std::move(init.minit))
    , touchedLits(solverState.values.size(), 0)
    , touchedClauses(solverState.formula.size(), NO_ID)
  { }

  // zwraca kolejny literał, który należy zawartościować na True, jeśli taki istnieje
  Literal decide() {
    return dec.decide();
  }

  // Dla napotkanego konfliktu zwraca nowo nauczoną klauzulę oraz nowy poziom decyzyjny
  std::tuple<Clause, int> analyse(SolverError err) {
    touchedLits.clear(); touchedClauses.clear();
    auto result = learn.analyse(err, touchedLits, touchedClauses);
    Clause& newClause = std::get<0>(result);
    int backtrackLevel = std::get<1>(result);
    newClause = mins.minimize(std::move(newClause));

    dec.handleConflict(err, touchedLits, touchedClauses);
    removal.handleConflict(err, touchedLits, touchedClauses);
    act.handleConflict(err, touchedLits, touchedClauses);

    return std::make_tuple(std::move(newClause), backtrackLevel);
  }

  // Uaktualnia swoją bazę dla nowej klauzuli
  void handleNewClause(const ClauseID id) {
    dec.handleNewClause(id);
    removal.handleNewClause(id);
    act.handleNewClause(id);
  }

  // Zwraca listę nauczonych klauzul do usunięcia z bazy, jeśli uzna to za konieczne
  std::vector<ClauseID> reduceDB() {
    return removal.reduceDB();
  }

  // zwraca akcję którą ma podjąć solver po ostatnio udanej propagacji
  Action nextAction() {
    return act.nextAction();
  }

  void handleAction(Action action) {
    dec.handleAction(action);
    removal.handleAction(action);
    act.handleAction(action);
  }

private:
  DecisionStrat dec;
  LearnStrat learn;
  RemovalStrat removal;
  ActionStrat act;
  MinimizerStrat mins;
  std::vector<Literal> touchedLits;     // literały dotknięte przez analizę konfliktu
  std::vector<ClauseID> touchedClauses; // klauzule dotkniete przez analize konfliktu
};

namespace DecisionS {

// Strategia składająca się z osobnychs trategii na wybór zmiennej i jej wartościowanie
template<typename VariableDec, typename PolarityDec>
struct Decision {
  struct Init {
    typename VariableDec::Init vinit;
    typename PolarityDec::Init pinit;
  };

  Decision(const SolverState& state, Decision::Init init)
    : vdec(state, std::move(init.vinit))
    , pdec(state, std::move(init.pinit))
  { }

  Literal decide() {
    const Variable var = vdec.decideVar();
    return pdec.decidePolarity(var);
  }

  void handleConflict(SolverError err,
                      const std::vector<Literal>& touchedLits,
                      const std::vector<ClauseID>& touchedClauses)
  {
    vdec.handleConflict(err, touchedLits, touchedClauses);
    pdec.handleConflict(err, touchedLits, touchedClauses);
  }

  void handleNewClause(ClauseID cid) {
    vdec.handleNewClause(cid);
    pdec.handleNewClause(cid);
  }

  void handleAction(Action act) {
    vdec.handleAction(act);
    pdec.handleAction(act);
  }

private:
  VariableDec vdec;
  PolarityDec pdec;
};

// Trzyma statystyki częstości dla negatywnych i pozytywnyh wystąpień zmiennej
struct PolarityStatistics {
  PolarityStatistics(int varCount);
  void clear();
  double ratio(Variable var) const; // procent występowania positives
  void add(Literal lit);

  std::vector<std::pair<double, double>> stats;
};

struct PhaseSavingPolarity {
  struct Init {
    bool clearBackups;     // czyści backupy na resecie
    bool clearStatistics;  // czyści statystyki na resecie
    bool clearConflicting; // czyści backupy dla skonfliktowanych zmiennych
    bool useRandom;        // używa randoma jednostajnego przy braku backupu, jeśli nie bierze ratio występowania jako rozkład
  };

  PhaseSavingPolarity(const SolverState& state, PhaseSavingPolarity::Init init);

  Literal decidePolarity(Variable var);

  void handleConflict(SolverError err,
                      const std::vector<Literal>& touchedLits,
                      const std::vector<ClauseID>& touchedClauses);

  void handleNewClause(ClauseID cid);

  void handleAction(Action act);

private:
  Literal decideWhenNoBackup(Variable var);
  void updateBackups(int untilLevel); // uaktualnia backupy do poziomu decyzyjnego

private:
  const SolverState& state;
  Init config;
  std::vector<Literal> backups;
  std::default_random_engine engine;
  PolarityStatistics statistics;
};

// Jeroslaw-Wang heuristics
struct JerWangPolarity {
  struct Init {
  };

  JerWangPolarity(const SolverState& state, JerWangPolarity::Init init);

  Literal decidePolarity(Variable var);

  void handleConflict(SolverError err,
                      const std::vector<Literal>& touchedLits,
                      const std::vector<ClauseID>& touchedClauses);

  void handleNewClause(ClauseID cid);

  void handleAction(Action act);

private:
  void update(const Clause& cl);

private:
  const SolverState& state;
  Init config;
  std::vector<std::pair<double, double>> stats;
};


struct VSIDS {
  struct Init {
    double decay;
    double threshold;
    double rescaler;
    double randPercent;  // procent losowych decyzyji
    bool useLearned;     // Czy zwiększa aktywności zmiennych z nauczoznej klauzuli
    bool initBySze;      // Czy inicjalizować aktywności inkrementując aktywność o 0.5 dla zmiennych z klauzul rozmiaru 2
  };

  VSIDS(const SolverState& state, VSIDS::Init config);

  void handleAction(Action act);
  Variable decideVar();
  void handleNewClause(ClauseID cid);
  void handleConflict(SolverError err,
                      const std::vector<Literal>& touchedLits,
                      const std::vector<ClauseID>& touchedClauses);

private:
  Variable randomChoice();
  Variable bestChoice();
  void rescaleActs();
  void decayActs();
  void bumpActivities(const std::vector<Literal>& lits);

private:
  const SolverState& state;
  Init config;
  std::vector<double> activities;
  double inc;
  std::default_random_engine engine;
  std::uniform_real_distribution<double> dist;
};

} // namespace Decision

namespace LearnS {

std::vector<Literal> calcReason(const SolverState& state, ClauseID cid, Literal lit);

struct FUIP {
  struct Init { };

  FUIP(const SolverState& state, FUIP::Init);
  std::tuple<Clause, int> analyse(SolverError err,
                                  std::vector<Literal>& touchedLits,
                                  std::vector<ClauseID>& touchedClauses);

private:
  const SolverState& state;
  std::vector<bool> seen;
};

} // namespace LearnS

namespace RemovalS {

// When Strategy musi dodatkowo posiadać funkcje:
// - shouldReduce(): bool
// - handleremove(const std::vector<ClauseID>&): void
template <typename WhenStrat, typename WhatStrat>
struct RemoveStrategy {
  struct Init {
    typename WhenStrat::Init whenInit;
    typename WhatStrat::Init whatInit;
  };
  RemoveStrategy(const SolverState& state, RemoveStrategy::Init init)
    : when(state, std::move(init.whenInit))
    , what(state, std::move(init.whatInit))
  { }

  void handleConflict(SolverError err,
                      const std::vector<Literal>& touchedLits,
                      const std::vector<ClauseID>& touchedClauses)
  {
    what.handleConflict(err, touchedLits, touchedClauses);
  }

  void handleNewClause(ClauseID cid) {
    what.handleNewClause(cid);
  }

  void handleAction(Action act) {
    when.handleAction(act);
    what.handleAction(act);
  }

  std::vector<ClauseID> reduceDB() {
    if (when.shouldReduce()) {
      return what.reduceDB();
    } else {
      return { };
    }
  }

  private:
    WhenStrat when;
    WhatStrat what;
};

// usuwa co maxConfl * (incMaxConfls^x) konfliktów, gdzie x - który raz to już robi
struct GeometricScheduling {
  struct Init {
    int maxConfl;
    double incMaxConfls;
  };

  GeometricScheduling(const SolverState&, GeometricScheduling::Init init);

  bool shouldReduce();
  void handleAction(Action act);
private:
  const SolverState& state;
  Init config;
  int lastConflicts;
  int mult;
};

// ususwa co b + a*x konfliktów, x - ile razy już usuwał
struct StaticScheduling {
  struct Init {
    int b;
    int a;
  };

  StaticScheduling(const SolverState&, StaticScheduling::Init init);

  bool shouldReduce();
  void handleAction(Action act);
private:
  const SolverState& state;
  Init config;
  int lastConflicts;
  int removals;
};

struct DynamicScheduling {
  struct Init {
    double maxLearnts; // procent rozmiaru oryginalnego problemu
    double incMaxLearnts;
  };

  DynamicScheduling(const SolverState& state, DynamicScheduling::Init init);

  bool shouldReduce();
  void handleAction(Action act);
private:
  const SolverState& state;
  Init config;
};

// usuwa najmniej aktywne klauzule - nie biorące ostatnio udziału w konfliktach
struct Activity {
  struct Init {
    double decay;
    double threshold;
    double rescaler;
    double removeFraction;
  };

  Activity(const SolverState& state, Activity::Init init);

  void handleConflict(SolverError err,
                      const std::vector<Literal>& touchedLits,
                      const std::vector<ClauseID>& touchedClauses);
  void handleNewClause(ClauseID cid);
  void handleAction(Action act);
  std::vector<ClauseID> reduceDB();

private:
  void bumpActivity(const ClauseID cid);
  void rescaleActs();
  void decayActs();

private:
  const SolverState& state;
  Init config;
  double inc;
  std::vector<std::pair<bool, double>> activities;
};

// Usuwa klauzule które, mają dużo zmiennych z różnyc poziomów decyzyjnych
struct LBD {
  struct Init {
    double removeFraction;
    bool updateTouched;
  };
  LBD(const SolverState& state, LBD::Init init);

  void handleConflict(SolverError err,
                      const std::vector<Literal>& touchedLits,
                      const std::vector<ClauseID>& touchedClauses);
  void handleNewClause(ClauseID cid);
  void handleAction(Action act);
  std::vector<ClauseID> reduceDB();

private:
  void updateLBD(ClauseID cid);

private:
  const SolverState& state;
  Init config;
  std::vector<std::pair<bool, int>> lbds;
  std::unordered_set<int> distinct; // <level, id>
};

// Usuwa część starych klauzul, które są dużej wielkości
struct BigOld {
  struct Init {
    double removeFraction;  // frakcja starych klauzul
    int size;           // minimalna wielkość usuwanych klauzul
  };

  BigOld(const SolverState& state, BigOld::Init init);
  void handleConflict(SolverError err,
                      const std::vector<Literal>& touchedLits,
                      const std::vector<ClauseID>& touchedClauses);
  void handleNewClause(ClauseID cid);
  void handleAction(Action act);
  std::vector<ClauseID> reduceDB();

private:
  void updateAge(ClauseID cid);

private:
  const SolverState& state;
  Init config;
  int64_t year;
  std::vector<std::pair<bool, int64_t>> ord;
};


} // namespace RemovalS


namespace ActionS {

struct SayNoTime {

  inline SayNoTime(int64_t interval) : interval(interval) { }
  inline void init() {
    start = std::chrono::high_resolution_clock::now();
  }
  inline void reset() {
    start = std::chrono::high_resolution_clock::now();
  }
  inline bool shouldSayNo() const {
    if (!interval) return false;

    const auto now = std::chrono::high_resolution_clock::now();
    if (std::chrono::duration_cast<std::chrono::seconds>(now - start).count() >= interval)
      return true;
    else
      return false;
  }
  const int64_t interval;
private:
  std::chrono::high_resolution_clock::time_point start;
};

struct Geometric {
  struct Init {
    int maxConflicts;
    double confMult;
    SayNoTime sayNo;
  };

  Geometric(const SolverState&, Geometric::Init init);

  void handleAction(Action act);
  void handleNewClause(ClauseID cid);
  void handleConflict(SolverError err,
                      const std::vector<Literal>& touchedLits,
                      const std::vector<ClauseID>& touchedClauses);
  Action nextAction();

private:
  const SolverState& state;
  Init config;
};

struct NestedGeometric { // dwa rosnące gemetryczne ciągi, drugi jest ograniczeniem pierwszego
  struct Init {
    int innerLimitBegin;
    int outerLimitBegin;
    double confMult;
  };

  NestedGeometric(const SolverState&, NestedGeometric::Init init);

  void handleAction(Action act);
  void handleNewClause(ClauseID cid);
  void handleConflict(SolverError err,
                      const std::vector<Literal>& touchedLits,
                      const std::vector<ClauseID>& touchedClauses);
  Action nextAction();

private:
  const SolverState& state;
  NestedGeometric::Init config;
  int innerLimit;
  int outerLimit;
};

struct Luby { // restarty oparte na ciągu Luby
  struct Init {
    double factor;
    SayNoTime sayNo;
  };
  Luby(const SolverState& state, Luby::Init init);

  void handleAction(Action act);
  void handleNewClause(ClauseID cid);
  void handleConflict(SolverError err,
                      const std::vector<Literal>& touchedLits,
                      const std::vector<ClauseID>& touchedClauses);
  Action nextAction();

private:
  const SolverState& state;
  Init config;
  int64_t seqPosMax;
  int64_t seqPos;
};

} // namespace Action

namespace MinimizationS {

  struct NoMinimization {
    struct Init { };
    inline NoMinimization(const SolverState&, NoMinimization::Init) { }
    inline Clause minimize(Clause cl) { return cl; }
  };

} // namespace MinimizationS
