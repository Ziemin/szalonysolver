#include <vector>
#include <algorithm>
#include <functional>
#include <random>
#include <cassert>
#include <iostream>
#include <climits>

#include "strategy.hpp"
#include "types.hpp"

namespace DecisionS { // =========================================================================================

PolarityStatistics::PolarityStatistics(int varCount)
  : stats(varCount+1, { 0, 0 })
  { }

void PolarityStatistics::clear() {
  stats.assign(stats.size(), { 0, 0 });
}

double PolarityStatistics::ratio(Variable var) const {
  auto st = stats[var];
  return st.first/(st.first + st.second);
}

void PolarityStatistics::add(Literal lit) {
  if (lit > 0) {
    stats[variable(lit)].first += 1;
  } else {
    stats[variable(lit)].second += 1;
  }
}

// ---------------------------------------------------------------------------------------------

PhaseSavingPolarity::PhaseSavingPolarity(const SolverState& state, PhaseSavingPolarity::Init init)
  : state(state)
  , config(init)
  , backups(state.varCount()+1, 0)
  , statistics(state.varCount())
{
  for (const Clause& cl: state.formula) {
    for (Literal l: cl) {
      statistics.add(l);
    }
  }
}

Literal PhaseSavingPolarity::decidePolarity(Variable var) {
  // najpierw uaktualniamy wcześniejszy poziom decyzyjny
  updateBackups(std::max(state.decLevel - 1, state.rootLevel));
  const Literal lit = backups[var];

  if (lit != 0) {
    return lit;
  } else {
    return decideWhenNoBackup(var);
  }
}

Literal PhaseSavingPolarity::decideWhenNoBackup(Variable var) {
  if (config.useRandom) {
    if (std::uniform_int_distribution<int>(0, 1)(engine) == 1)
      return -var;
    else
      return var;
  } else {
    if (std::bernoulli_distribution(statistics.ratio(var))(engine))
      return var;
    else
      return -var;
  }
}

void PhaseSavingPolarity::updateBackups(int untilLevel) {
  // wystarczy to robić tylko dla ostatniego poziomu decyzyjnego
  for (int i = state.trail.size()-1; i >= 0; i--) {
    Literal lit = state.trail[i];
    if (state.levels[variable(lit)] < untilLevel)
      break;
    backups[variable(lit)] = state.values[variable(lit)];
  }
}

void PhaseSavingPolarity::handleConflict(SolverError,
                    const std::vector<Literal>&,
                    const std::vector<ClauseID>&)
{
  if (config.clearConflicting) { // czyścimy backupy dla ostatniego levela
    for (int i = state.trail.size()-1; i >= 0; i--) {
      Literal lit = state.trail[i];
      if (state.levels[variable(lit)] != state.decLevel)
        break;
      backups[variable(lit)] = 0;
    }
  } else { // aktualizujemy do najnowszych
    updateBackups(state.decLevel);
  }
}

void PhaseSavingPolarity::handleNewClause(ClauseID cid) {
  for (Literal l: state.learnt[cid]) {
    statistics.add(l);
  }
}

void PhaseSavingPolarity::handleAction(Action act) {
  if (act == Action::RESTART) {
    if (config.clearBackups) {
      backups.assign(state.varCount()+1, 0);
    }
    if (config.clearStatistics) {
      statistics.clear();
    }
  }
}

// ---------------------------------------------------------------------------------------------
//
JerWangPolarity::JerWangPolarity(const SolverState& state, JerWangPolarity::Init init)
  : state(state)
  , config(init)
  , stats(state.varCount()+1, {0 , 0})
{
  for (const Clause& cl: state.formula)
    update(cl);
}

Literal JerWangPolarity::decidePolarity(Variable var) {
  const auto st = stats[var];
  if (st.first > st.second) return var;
  else return -var;
}

void JerWangPolarity::update(const Clause& cl) {
  double added = 1;
  const uint64_t siz = cl.size();
  const uint64_t bigs = siz/64;
  const uint64_t smalls = siz % 64;
  for (int i = 0; i < (int)bigs; i++) {
    added *= 1.0/((double)(1UL << 63)) * 0.5;
  }
  added *= 1.0/((double)(1UL << smalls)) * 0.5;

  for (Literal lit: cl) {
    if (lit > 0) stats[variable(lit)].first += added;
    else stats[variable(lit)].second += added;
  }
}

void JerWangPolarity::handleConflict(SolverError,
                    const std::vector<Literal>&,
                    const std::vector<ClauseID>&)
{ }

void JerWangPolarity::handleNewClause(ClauseID cid) {
  update(state.learnt[cid]);
}

void JerWangPolarity::handleAction(Action) { }

// ---------------------------------------------------------------------------------------------

VSIDS::VSIDS(const SolverState& state, VSIDS::Init config)
  : state(state)
  , config(std::move(config))
  , activities(state.varCount()+1, 0)
  , inc(1)
  , dist(0, 100)
{
  activities[0] = -1;

  if (config.initBySze) {
    for (const Clause& cl: state.formula) {
      if (cl.size() != 2)
        continue;
      for (Literal lit: cl) {
        const Variable var = variable(lit);
        if (activities[var] > 0) // aktualizujemy tylko raz
          continue;
        activities[var] += 0.5;
      }
    }
  }
}

void VSIDS::handleAction(Action /* act */) { }

Variable VSIDS::decideVar() {
  if (dist(engine) <= config.randPercent)
    return randomChoice();
  else
    return bestChoice();
}

Variable VSIDS::randomChoice() {
  std::vector<int> freeVars;
  for (int v = 1; v <= state.varCount(); v++) {
    if (!state.hasValue(v))
      freeVars.push_back(v);
  }
  assert(!freeVars.empty());

  const int ind = std::uniform_int_distribution<int>(0, (int)freeVars.size()-1)(engine);
  return freeVars[ind];
}

Variable VSIDS::bestChoice() {
  int bestVar = 0;
  for (int v = 1; v < (int)activities.size(); v++) {
    if (activities[v] > activities[bestVar] && !state.hasValue(v)) {
      bestVar = v;
    }
  }
  assert(bestVar != 0);

  return bestVar;
}

void VSIDS::handleConflict(SolverError,
                                const std::vector<Literal>& touchedLits,
                                const std::vector<ClauseID>&)
{
  bumpActivities(touchedLits);
  if (!config.useLearned) { // w przeciwynym wypadku czekamy na nową klauzulę do zdecayowania
    decayActs();
  }
}

void VSIDS::bumpActivities(const std::vector<Literal>& lits) {
  for (Literal lit: lits) {
    activities[variable(lit)] += inc;
  }
  for (Literal lit: lits) {
    if (activities[variable(lit)] > config.threshold)
     rescaleActs();
  }
}

void VSIDS::handleNewClause(ClauseID cid) {
  const Clause& cl = state.learnt[cid];
  if (config.useLearned) {
    bumpActivities(cl);
    decayActs();
  }
}

void VSIDS::rescaleActs() {
  for (int v = 1; v <= (int)state.varCount(); v++)
    activities[v] *= config.rescaler;
  inc *= config.rescaler;
}

void VSIDS::decayActs() {
  inc *= config.decay;
}

// ---------------------------------------------------------------------------------------------

} // namespace DecisionS


namespace LearnS { // =========================================================================================

std::vector<Literal> calcReason(const SolverState& state, ClauseID cid, Literal p) {
  std::vector<Literal> reason;
  const Clause& cl = cid.learnt ? state.learnt[cid] : state.formula[cid.ind];
  for (Literal lit: cl) {
    if (lit != p) {
      reason.push_back(-lit);
      assert(state.values[variable(lit)] == -lit);
    }
  }
  return reason;
}

FUIP::FUIP(const SolverState& state, FUIP::Init)
  : state(state)
  , seen(state.varCount()+1, false)
{ }

std::tuple<Clause, int> FUIP::analyse(SolverError err,
                                      std::vector<Literal>& touchedLits,
                                      std::vector<ClauseID>& touchedClauses)
{
  Clause newCl;

  seen.assign(seen.size(), false);
  int bcLevel = state.rootLevel;
  ClauseID confl = err.cid;
  Literal p = 0;

  touchedClauses.push_back(confl);
  auto reason = calcReason(state, confl, p);
  auto handleReason = [&](const std::vector<Literal>& reason) {
    for (Literal q: reason)
      if (!seen[variable(q)]) {
        touchedLits.push_back(q);
        seen[variable(q)] = true;
        const int lev = state.levels[variable(q)];
        if (lev != state.decLevel && lev > state.rootLevel) {
          newCl.push_back(-q);
          bcLevel = std::max(bcLevel, lev);
        }
      }
  };
  handleReason(reason);

  for (int ind = state.trail.size()-1; ind >= 0 && state.levels[variable(state.trail[ind])] == state.decLevel; ind--) {
    Literal lit = state.trail[ind];
    if (!seen[variable(lit)]) continue;
    p = lit;
    touchedLits.push_back(p);
    confl = state.reasons[variable(p)];
    if (!confl.isNull()) {
      assert(state.isLocked(confl));
      touchedClauses.push_back(confl);
      handleReason(calcReason(state, confl, p));
    }
  }

  assert(p != 0);
  newCl.push_back(-p);
  std::reverse(newCl.begin(), newCl.end());
  return std::make_tuple(newCl, bcLevel);
}

// ---------------------------------------------------------------------------------------------

} // namespace LearnS

namespace RemovalS { // =========================================================================================

GeometricScheduling::GeometricScheduling(const SolverState& state, GeometricScheduling::Init init)
  : state(state)
  , config(init)
  , lastConflicts(0)
  , mult(config.maxConfl)
{ }

bool GeometricScheduling::shouldReduce() {
  if (state.conflicts - lastConflicts >= mult) {
    mult *= config.incMaxConfls;
    lastConflicts = state.conflicts;
    return true;
  } else {
    return false;
  }
}

void GeometricScheduling::handleAction(Action act) {
  if (act == Action::RESTART) {
    mult = config.maxConfl;
    lastConflicts = 0;
    config.maxConfl *= config.incMaxConfls;
  }
}

// ---------------------------------------------------------------------------------------------

StaticScheduling::StaticScheduling(const SolverState& state, StaticScheduling::Init init)
  : state(state)
  , config(init)
  , lastConflicts(0)
  , removals(0)
{ }

bool StaticScheduling::shouldReduce() {
  if (state.conflicts - lastConflicts >= config.b + config.a * removals) {
    removals++;
    lastConflicts = state.conflicts;
    return true;
  } else {
    return false;
  }
}

void StaticScheduling::handleAction(Action act) {
  if (act == Action::RESTART) {
    removals = 0;
    lastConflicts = 0;
  }
}

// ---------------------------------------------------------------------------------------------

DynamicScheduling::DynamicScheduling(const SolverState& state, DynamicScheduling::Init init)
  : state(state)
  , config(init)
{ }

bool DynamicScheduling::shouldReduce() {
  if (state.learntCount >= config.maxLearnts * state.formula.size()) {
    return true;
  } else {
    return false;
  }
}

void DynamicScheduling::handleAction(Action act) {
  if (act == Action::RESTART) {
    config.maxLearnts *= config.incMaxLearnts;
  }
}

// ---------------------------------------------------------------------------------------------

Activity::Activity(const SolverState& state, Activity::Init init)
: state(state)
, config(std::move(init))
, inc(1)
{

}

void Activity::handleConflict(SolverError,
                              const std::vector<Literal>&,
                              const std::vector<ClauseID>& touchedClauses)
{
  for (const ClauseID& cid: touchedClauses) {
    bumpActivity(cid);
  }
}

void Activity::handleNewClause(ClauseID cid) {
  if (cid.ind >= (int)activities.size()){
    activities.resize(cid.ind+1, { false, 0 });
  }
  activities[cid.ind].first = true;
  bumpActivity(cid);
}

std::vector<ClauseID> Activity::reduceDB() {
  std::vector<std::pair<double, int>> alive;
  std::vector<ClauseID> toRemove;

  for (int i = 0; i < (int)activities.size(); i++) {
    if (activities[i].first && !state.isLocked({i, true})) {
      alive.push_back({activities[i].second, i});
    }
  }

  sort(begin(alive), end(alive));
  const int removedCount = (int)alive.size() * config.removeFraction;

  assert(removedCount <= state.learntCount);
  assert(removedCount <= (int)alive.size());

  for (int i = 0; i < removedCount; i++) {
    toRemove.push_back({ alive[i].second, true });
    activities[alive[i].second] = { false, 0 };
  }
  return toRemove;
}

void Activity::handleAction(Action) { }

void Activity::bumpActivity(const ClauseID cid) {
  if (cid.isNull() || !cid.learnt || !activities[cid.ind].first)
    return;
  activities[cid.ind].second += inc;
  if (activities[cid.ind].second >= config.threshold)
    rescaleActs();
}

void Activity::rescaleActs() {
  for(auto& act: activities)
    act.second *= config.rescaler;
  inc *= config.rescaler;
}

void Activity::decayActs() {
  inc *= config.decay;
}
// ---------------------------------------------------------------------------------------------

LBD::LBD(const SolverState& state, LBD::Init init)
  : state(state)
  , config(init)
{ }

void LBD::handleConflict(SolverError,
                    const std::vector<Literal>&,
                    const std::vector<ClauseID>& touchedClauses) {
  if (config.updateTouched) {
    for (ClauseID cid: touchedClauses)
      updateLBD(cid);
  }
}

void LBD::handleNewClause(ClauseID cid) {
  if (cid.ind >= (int)lbds.size()) {
    lbds.resize(cid.ind+1, { false, INT_MAX });
  }
  lbds[cid.ind].first = true;
  updateLBD(cid);
}
void LBD::handleAction(Action) { }

std::vector<ClauseID> LBD::reduceDB() {
  std::vector<std::pair<double, int>> alive;
  std::vector<ClauseID> toRemove;

  for (int i = 0; i < (int)lbds.size(); i++) {
    if (lbds[i].first && lbds[i].second != 2 && !state.isLocked({i, true})) {
      alive.push_back({lbds[i].second, i});
    }
  }

  sort(alive.rbegin(), alive.rend());
  const int removedCount = alive.size() * config.removeFraction;
  assert(removedCount <= (int)alive.size());

  for (int i = 0; i < removedCount; i++) {
    toRemove.push_back({ alive[i].second, true });
    lbds[alive[i].second] = { false, INT_MAX };
  }
  return toRemove;
}

void LBD::updateLBD(ClauseID cid) {
  if (cid.isNull() || !cid.learnt || !lbds[cid.ind].first) {
    return;
  }
  for (Literal lit: state.learnt[cid]) {
    distinct.insert(state.levels[variable(lit)]);
  }
  lbds[cid.ind].second = distinct.size();
  for (Literal lit: state.learnt[cid]) {
    distinct.erase(state.levels[variable(lit)]);
  }
}

// ---------------------------------------------------------------------------------------------

BigOld::BigOld(const SolverState& state, BigOld::Init init)
  : state(state)
  , config(init)
  , year(0)
{ }

void BigOld::handleConflict(SolverError,
                    const std::vector<Literal>&,
                    const std::vector<ClauseID>&)
{ }

void BigOld::handleNewClause(ClauseID cid) {
  year++;
  if (cid.ind >= (int)ord.size()) {
    ord.resize(cid.ind+1, { false, 0 });
  }
  ord[cid.ind].first = true;
  updateAge(cid);
}

void BigOld::handleAction(Action) { }

std::vector<ClauseID> BigOld::reduceDB() {
  std::vector<std::pair<double, int>> alive;
  std::vector<ClauseID> toRemove;

  for (int i = 0; i < (int)ord.size(); i++) {
    if (ord[i].first && (int)state.learnt[{i, true}].size() >= config.size && !state.isLocked({i, true})) {
      alive.push_back({ord[i].second, i});
    }
  }

  sort(alive.begin(), alive.end());
  const int removedCount = alive.size() * config.removeFraction;
  assert(removedCount <= (int)alive.size());

  for (int i = 0; i < removedCount; i++) {
    toRemove.push_back({ alive[i].second, true });
    ord[alive[i].second] = { false, 0 };
  }
  return toRemove;
}

void BigOld::updateAge(ClauseID cid) {
  if (cid.isNull() || !cid.learnt || !ord[cid.ind].first) {
    return;
  }
  ord[cid.ind].second = year;
}

// ---------------------------------------------------------------------------------------------

} // namesapce RemovalS

namespace ActionS { // =========================================================================================

Geometric::Geometric(const SolverState& state, Geometric::Init init)
  : state(state), config(std::move(init))
{
  config.sayNo.init();
}

void Geometric::handleAction(Action act) {
  switch (act) {
    case Action::RESTART:
      config.maxConflicts *= config.confMult;
      break;
    default:
      break;
  }
}

void Geometric::handleNewClause(ClauseID) { }

void Geometric::handleConflict(SolverError, const std::vector<Literal>&, const std::vector<ClauseID>&) {
}

Action Geometric::nextAction() {
  if (config.sayNo.shouldSayNo())
    return Action::SAY_NO;

  if (state.conflicts > config.maxConflicts)
    return Action::RESTART;
  else
    return Action::CONTINUE;
}

// ---------------------------------------------------------------------------------------------

NestedGeometric::NestedGeometric(const SolverState& state, NestedGeometric::Init init)
  : state(state)
  , config(init)
  , innerLimit(config.innerLimitBegin)
  , outerLimit(config.outerLimitBegin)
{ }

void NestedGeometric::handleAction(Action) { }

void NestedGeometric::handleNewClause(ClauseID) { }

void NestedGeometric::handleConflict(SolverError, const std::vector<Literal>&, const std::vector<ClauseID>&) {
}

Action NestedGeometric::nextAction() {
  if (state.conflicts > innerLimit) {
    innerLimit *= config.confMult;
    if (innerLimit > config.outerLimitBegin) {
      innerLimit = config.innerLimitBegin;
      outerLimit *= config.confMult;
    }
    return Action::RESTART;
  }
  else
    return Action::CONTINUE;
}

// ---------------------------------------------------------------------------------------------

Luby::Luby(const SolverState& state, Luby::Init init)
  : state(state)
  , config(init)
  , seqPosMax(2)
  , seqPos(0)
{
  config.sayNo.init();
}

void Luby::handleAction(Action) { }
void Luby::handleNewClause(ClauseID) { }
void Luby::handleConflict(SolverError,
                        const std::vector<Literal>&,
                        const std::vector<ClauseID>&)
{ }

Action Luby::nextAction() {
  if (config.sayNo.shouldSayNo()) {
    return Action::SAY_NO;
  }
  const int mult = seqPos == 0 ? 0 : seqPos-1;
  if (state.conflicts >= (1 << mult) * config.factor) {
    seqPos++;
    if (seqPos > seqPosMax) {
      seqPos = 0;
      seqPosMax++;
    }
    return Action::RESTART;
  } else {
    return Action::CONTINUE;
  }
}

} // namespace ActionS

namespace MinimizationS { // =========================================================================================

} // namespace MinimizationS
