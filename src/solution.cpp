#include <iostream>
#include <cassert>

#include "types.hpp"
#include "input.hpp"
#include "strategy.hpp"
#include "solver.hpp"
#include "config.hpp"


int main() {

  int z;
  std::cin >> z;
  while (z--) {
    CNF cnf = readProblemSatori(std::cin);

    Solver<StratType> solver(cnf, std::move(stratConf));

    PBindings result = solver.solve();
    if (result.empty()) {
      std::cout << "UNSAT" << std::endl;
    } else {
      assert(isSatisfied(cnf, result));
      std::cout << "SAT" << std::endl;
      for (int i = 1; i < (int)result.size(); i++) {
        if (result[i]) std::cout << result[i] << " ";
      }
      std::cout << 0 << std::endl;
    }
  }


  return 0;
}
