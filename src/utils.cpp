#include <vector>
#include <set>
#include <algorithm>
#include <numeric>
#include "types.hpp"
#include "utils.hpp"

Clause simplifyClause(const Clause& clause) {
  Clause newClause;
  auto literals = std::set<Literal>(begin(clause), end(clause));

  for (Literal lit: literals) {
    if (literals.find(-lit) != end(literals)) {
      newClause.clear();
      break;
    }
    newClause.push_back(lit);
  }

  return newClause;
}

CNF simplifyCNF(const CNF& cnf) {
  CNF newCnf;

  for (const Clause& cl: cnf) {
    Clause newClause = simplifyClause(cl);

    if (!newClause.empty())
      newCnf.push_back(move(newClause));
  }

  return newCnf;
}

Variable findMax(const Clause& cl) {
  auto it = std::max_element(begin(cl), end(cl),
      [](Literal a, Literal b) { return variable(a) < variable(b); });
  if (it == end(cl)) return 0;
  else return variable(*it);
}

Variable findMax(const CNF& cnf) {
  Variable maxVar = 0;
  for (const Clause& cl: cnf)
    maxVar = std::max(maxVar, findMax(cl));
  return maxVar;
}
