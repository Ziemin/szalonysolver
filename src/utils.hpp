#pragma once

#include "types.hpp"

Clause simplifyClause(const Clause& clause);

// upraszcza instancję, usuwa puste klauzule, klauzule trywialnie spełnione
// zduplikowane literały
CNF simplifyCNF(const CNF& cnf);


Variable findMax(const Clause& cl);

Variable findMax(const CNF& cnf);
