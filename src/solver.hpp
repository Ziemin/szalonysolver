#pragma once

#include <vector>
#include <queue>
#include <tuple>
#include <algorithm>
#include <cassert>
#include "types.hpp"
#include "utils.hpp"

SolverState initSolverState(CNF cnf);

using Watched = std::vector<ClauseID>;
struct Watches : public std::vector<Watched> {

  Watches(const CNF& cnf, int varCount);

  Watched& operator[](Literal lit);
  const Watched& operator[](Literal lit) const;

  // usuwa klazulę z obserwowanych
  void removeClause(const Clause& cl, ClauseID cid);

  void addClause(const Clause& cl, ClauseID cid);
};

struct PropQue {

  inline PropQue(int size) : beg(0), end(0), que(new Literal[size+1]) { }
  inline ~PropQue() { delete [] que; }

  inline void push(Literal lit) {
    que[end++] = lit;
  }
  inline Literal front() const {
    return que[beg];
  }
  inline void pop() {
    beg++;
  }
  inline void clear() {
    beg = end = 0;
  }
  inline bool empty() const {
    return beg == end;
  }
  inline bool size() const {
    return end - beg;
  }

private:
  int beg, end;
  Literal* que;
};

/*
 * Rdzeń całego solvera, niezależny od konkretnych heurystyk
 */
template <typename Strategy>
struct Solver {

  Solver(CNF cnf, typename Strategy::Init init)
  : state(initSolverState(move(cnf)))
  , watches(state.formula, state.varCount())
  , strategy(state, std::move(init))
  , propQue(state.varCount())
  { }

  // zwraca puste w przypadku błędu
  // jeśli natomiast cały model zawiera dowolne wartościowanie zmiennych
  // to zwraca bindingi o rozmiarze 1
  PBindings solve();

  inline bool isSolved() const {
    assert(state.assignedCount <= state.varCount());
    return state.assignedCount == state.varCount();
  }

private:

  void assign(Literal lit, ClauseID reasoncClause);

  void cancelOne();

  // zwraca ID nowo nauczonej klauzuli
  ClauseID learnClause(Clause clause);

  void removeClauses(std::vector<ClauseID> clauses);

  SolverError enqueue(Literal lit, ClauseID reason);

  SolverError runQueue();

  SolverError propagateLit(Literal lit);

  SolverError propagateClause(Literal lit, ClauseID cid);

  void restartSolver();

  void cancelUntil(int level);

  void debugPrintState();

private:
  SolverState state;
  Watches watches;
  Strategy strategy;
  PropQue propQue;
};

template<typename Strategy>
void Solver<Strategy>::debugPrintState() {
  std::cerr << "==== Solver State ====" << std::endl
            << "CNF: " << state.formula << std::endl
            << "DecLevel: " << state.decLevel << std::endl
            << "Trail: ";
  std::cerr << "[";
  for (int i = 0; i < (int)state.trail.size(); i++) {
    std::cerr << state.trail[i];
    if (i < (int)state.trail.size()-1) std::cerr << ",";
  }
  std::cerr << "]" << std::endl;
  std::cerr << "Bindings: ";
  std::cerr << "[";
  for (int i = 0; i < (int)state.values.size(); i++) {
    std::cerr << state.values[i];
    if (i < (int)state.values.size()-1) std::cerr << ",";
  }
  std::cerr << "]" << std::endl;

  std::cerr << "Levels: ";
  std::cerr << "[";
  for (int i = 0; i < (int)state.levels.size(); i++) {
    std::cerr << state.levels[i];
    if (i < (int)state.levels.size()-1) std::cerr << ",";
  }
  std::cerr << "]" << std::endl;

  std::cerr << "Queue front: " << (propQue.empty() ? 0 : propQue.front()) << " Queue size: " << propQue.size() << std::endl;
  for (int v = 1; v <= state.varCount(); v++) {
    std::cerr << "Watches for: " << -v << std::endl;
    for (int i = 0; i < (int)watches[-v].size(); i++) {
      const ClauseID wid = watches[-v][i];
      const Clause& cl = wid.learnt ? state.learnt[wid] : state.formula[wid.ind];
      std::cerr << cl << ", ";
    }
    std::cerr << std::endl;
    std::cerr << "Watches for: " << v << std::endl;
    for (int i = 0; i < (int)watches[v].size(); i++) {
      const ClauseID wid = watches[v][i];
      const Clause& cl = wid.learnt ? state.learnt[wid] : state.formula[wid.ind];
      std::cerr << cl << ", ";
    }
    std::cerr << std::endl;
  }
  std::cerr << std::endl;

}

template<typename Strategy>
PBindings Solver<Strategy>::solve() {

  // kolejkujemy unity już na początku
  for (const Clause& cl: state.formula) {
    if (cl.empty()) return {};
    else if (cl.size() == 1) {
      if (enqueue(cl[0], NO_ID))
        return {}; // upadek już na samym starcie
    }
    assert(!cl.empty());
  }

  while (true) {
    auto err = runQueue();

    if (!err) {
      // zwracamy rozwiązanie
      if (isSolved()) return state.values;

      removeClauses(strategy.reduceDB());

      const auto act = strategy.nextAction();
      switch (act) {
        case Action::RESTART:
          restartSolver();
          break;
        case Action::SAY_NO:
          return {};
        case Action::CONTINUE:
          break;
      }
      strategy.handleAction(act);

      state.decLevel++;
      enqueue(strategy.decide(), NO_ID);

    } else {
      state.conflicts++;
      if (state.decLevel == state.rootLevel)
        return {}; // błąd, unsat

      Clause newClause;
      int backtrackLevel;
      std::tie(newClause, backtrackLevel) = strategy.analyse(err);

      ClauseID newCid = learnClause(newClause);
      if (!newCid.isNull())
        strategy.handleNewClause(newCid);
      cancelUntil(std::max(backtrackLevel, state.rootLevel));

      enqueue(newClause[0], newCid);
    }
  }
}

template<typename Strategy>
void Solver<Strategy>::assign(Literal lit, ClauseID reasonClause) {
  const int varl = variable(lit);
  state.values[varl] = lit;
  state.reasons[varl] = reasonClause;
  state.assignedCount++;
  state.trail.push_back(lit);
  state.levels[varl] = state.decLevel;
}

template<typename Strategy>
void Solver<Strategy>::cancelOne() {
  const int varl = variable(state.trail.back());
  state.values[varl] = 0;
  state.levels[varl] = NO_LEVEL;
  state.reasons[varl] = NO_ID;
  state.assignedCount--;
  state.trail.pop_back();
}

// zwraca ID nowo nauczonej klauzuli
template<typename Strategy>
ClauseID Solver<Strategy>::learnClause(Clause clause) {
  ClauseID newCid = NO_ID;

  if (clause.size() >= 2) {
    newCid = state.learnt.addClause(std::move(clause));
    const Clause& newCl = state.learnt[newCid];
    watches[-newCl[0]].push_back(newCid);
    watches[-newCl[1]].push_back(newCid);
    state.learntCount++;
  }

  return newCid;
}

template<typename Strategy>
void Solver<Strategy>::removeClauses(std::vector<ClauseID> clauses) {
  if(clauses.empty())
    return;
  state.learntCount -= clauses.size();
  for (ClauseID cid: clauses) {
    watches.removeClause(state.learnt[cid], cid);
  }
  state.learnt.removeClauses(std::move(clauses));
}

template<typename Strategy>
SolverError Solver<Strategy>::enqueue(Literal lit, ClauseID reason) {
  // already set to True
  const int val = state.values[variable(lit)];
  if (val == lit) return NO_ERROR;
  else if (val == 0) {
    assign(lit, reason);
    propQue.push(lit);
    return NO_ERROR;
  } else
    return { lit, reason };
}

template<typename Strategy>
SolverError Solver<Strategy>::runQueue() {
  SolverError err = NO_ERROR;
  while (!err && !propQue.empty()) {
    const Literal lit = propQue.front();
    propQue.pop();
    err = propagateLit(lit);
  }

  std::queue<Literal> empty;
  propQue.clear(); // czyścimy kolejkę
  return err;
}

template<typename Strategy>
SolverError Solver<Strategy>::propagateLit(Literal lit) {
  Watched oldWatched = watches[lit];
  watches[lit].clear();
  while (!oldWatched.empty()) {
    const ClauseID wCid = oldWatched.back();
    oldWatched.pop_back();

    const SolverError err = propagateClause(lit, wCid);
    if (err) {
      // być może propagateClauese dodała watche dla tego literału do watches[lit]
      std::copy(begin(watches[lit]), end(watches[lit]), std::back_inserter(oldWatched));
      watches[lit] = std::move(oldWatched); // przywracamy stare watche
      return err;
    }
  }
  return NO_ERROR;
}

template<typename Strategy>
SolverError Solver<Strategy>::propagateClause(Literal lit, ClauseID cid) {
  Clause& cl = cid.learnt ? state.learnt[cid] : state.formula[cid.ind];
  assert(cl[1] == -lit || cl[0] == -lit);
  if (cl[0] == -lit) {
    std::swap(cl[0], cl[1]);
  }

  if (state.isTrue(cl[0])) {
    watches[lit].push_back(cid);
    return NO_ERROR;
  }

  for (int i = 2 ; i < (int)cl.size(); i++) {
    if (!state.isFalse(cl[i])) {
      std::swap(cl[1], cl[i]);
      watches[-cl[1]].push_back(cid);
      return NO_ERROR;
    }
  }
  watches[lit].push_back(cid);
  return enqueue(cl[0], cid);
}

template<typename Strategy>
void Solver<Strategy>::restartSolver() {
  cancelUntil(state.rootLevel);
  state.conflicts = 0;
  state.restarts++;
}

template<typename Strategy>
void Solver<Strategy>::cancelUntil(int level) {

  while (!state.trail.empty() && state.levels[variable(state.trail.back())] > level)
  {
    cancelOne();
  }
  state.decLevel = level;
}
