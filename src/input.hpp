#pragma once

#include <iostream>

#include "types.hpp"

CNF readProblem(std::istream& is);

CNF readProblemSatori(std::istream& is);
