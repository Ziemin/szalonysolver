#include <iostream>
#include <sstream>
#include <locale>

#include "input.hpp"

CNF readProblem(std::istream& is) {
  CNF cnf;

  std::string line;
  while (std::getline(is, line)) {
    int firstD = 0;
    for (int i = 0; i < (int)line.size(); i++)
      if (line[i] != ' ') {
        firstD = i;
        break;
      }
    if (line.empty() || !(std::isdigit(line[firstD]) || line[firstD] == '-'))
      continue;

    Clause clause;

    std::stringstream ss(line);
    Literal lit;

    while(ss >> lit) {
      if (lit == 0) break;
      clause.push_back(lit);
    }

    if (!clause.empty())
      cnf.push_back(move(clause));
  }

  return cnf;
}

CNF readProblemSatori(std::istream& is) {
  CNF cnf;

  std::string line;
  is >> line >> line;
  int z, k;
  is >> z >> k;

  for (int i = 0; i < k; i++) {
    int l = 0;
    Clause clause;
    while(is >> l, l != 0) {
      clause.push_back(l);
    }
    cnf.push_back(std::move(clause));
  }

  return cnf;
}
