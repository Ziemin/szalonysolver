#include <iostream>
#include <vector>
#include <algorithm>
#include <cassert>

#include "types.hpp"


bool Clause::operator()(const FBindings& binds) const {
  return std::any_of(begin(), end(),
      [&binds](Literal lit) {
        return sign(lit) == binds[variable(lit)];
      });
}

std::ostream& operator<<(std::ostream& out, const Clause& clause) {
  out << "(";
  for (size_t v = 0; v < clause.size() - 1; v++)
    out << clause[v] << " v ";
  if (!clause.empty()) out << *(clause.crbegin());
  out << ") ";

  return out;
}

bool CNF::operator()(const FBindings& binds) const {
  bool isTrue = true;
  for(const Clause& clause: *this) {
    isTrue = clause(binds);
    if (!isTrue) break;
  }
  return isTrue;
}

std::ostream& operator<<(std::ostream& out, const CNF& cnf) {
  for (size_t v = 0; v < cnf.size() - 1; v++)
    out << cnf[v] <<" /\\ ";

  if (!cnf.empty()) out << *(cnf.crbegin());

  return out;
}

bool isSatisfied(const Clause& cnf, const FBindings& binds) {
  return std::any_of(begin(cnf), end(cnf),
        [&](Literal lit) {
            return isSatisfied(lit, binds);
        });
}

bool isSatisfied(const CNF& cnf, const FBindings& binds) {
  return std::all_of(begin(cnf), end(cnf),
            [&](const Clause& cl) {
                return isSatisfied(cl, binds);
            });
}

bool isSatisfied(const Clause& cnf, const PBindings& binds) {
  return std::any_of(begin(cnf), end(cnf),
        [&](Literal lit) {
            assert(lit != 0);
            return binds[variable(lit)] == lit;
        });
}

bool isSatisfied(const CNF& cnf, const PBindings& binds) {
  return std::all_of(begin(cnf), end(cnf),
            [&](const Clause& cl) {
                return isSatisfied(cl, binds);
            });
}

// -- CNFLearn implementation

Clause& CNFLearnt::operator[](ClauseID cid) {
  assert(!(*this)[cid.ind].empty());
  return (*this)[cid.ind];
}
const Clause& CNFLearnt::operator[](ClauseID cid) const {
  assert(!(*this)[cid.ind].empty());
  return (*this)[cid.ind];
}

void CNFLearnt::removeClauses(std::vector<ClauseID> toRemove) {
  for (ClauseID cid: toRemove) {
    freeInds.push_back(cid.ind);
    (*this)[cid.ind].clear();
  }
}

ClauseID CNFLearnt::addClause(Clause clause) {
  ClauseID newInd = NO_ID;
  if (freeInds.empty()) {
    push_back(move(clause));
    newInd = { (int)size()-1, true };
  } else {
    newInd = { freeInds.back(), true };
    freeInds.pop_back();
    (*this)[newInd.ind] = move(clause);
  }
  return newInd;
}

// -- SolverError
SolverError NO_ERROR = SolverError();


